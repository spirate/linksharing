<?php

Route::group(['middleware' => 'web', 'prefix' => 'linksharing', 'namespace' => 'Modules\LinkSharing\Http\Controllers'], function()
{
    Route::get('/', 'LinkSharingController@index');
    Route::get('create', 'LinkSharingController@create')->middleware('auth');
    Route::post('linksharing/create', 'LinkSharingController@store')->middleware('auth');
    Route::delete('linksharing/destroy/{id}', ['as' => 'storage-linkasharing', 'uses' => 'LinkSharingController@destroy'])->middleware('auth');
});


Route::middleware('api')->get('/open', function (Request $request) {
    dd('hola');
});