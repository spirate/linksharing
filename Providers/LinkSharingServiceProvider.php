<?php
/**
 * Copyright (c) 2017.  Modulo Creado por: Kelly Salazar (VdjKelly)
 */

namespace Modules\LinkSharing\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\LinkSharing\Contracts\PostsCategoriesServiceContract;
use Modules\LinkSharing\Contracts\PostsCommentServiceContract;
use Modules\LinkSharing\Contracts\PostsServiceContract;
use Modules\LinkSharing\Contracts\PostsVotesServiceContract;
use Modules\LinkSharing\Repositories\PostsServices;
use Modules\LinkSharing\Services\PostsCategoriesService;
use Modules\LinkSharing\Services\PostsCommentService;
use Modules\LinkSharing\Services\PostsVotesService;

class LinkSharingServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PostsServiceContract::class, PostsServices::class);
        $this->app->bind(PostsCommentServiceContract::class, PostsCommentService::class);
        $this->app->bind(PostsCategoriesServiceContract::class, PostsCategoriesService::class);
        $this->app->bind(PostsVotesServiceContract::class, PostsVotesService::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('linksharing.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'linksharing'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = base_path('resources/views/modules/linksharing');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/linksharing';
        }, \Config::get('view.paths')), [$sourcePath]), 'linksharing');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = base_path('resources/lang/modules/linksharing');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'linksharing');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'linksharing');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
