<?php
/**
 * Copyright (c) 2017.  Modulo Creado por: Kelly Salazar (VdjKelly)
 */

if (!app()->routesAreCached()) {
    require __DIR__ . '/Http/routes.php';
}

