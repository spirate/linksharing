<?php
/**
 * Copyright (c) 2017.  Modulo Creado por: Kelly Salazar (VdjKelly)
 */

/**
 * Created by PhpStorm.
 * User: vdjkelly
 * Date: 27/03/17
 * Time: 06:08 AM
 */

namespace Modules\LinkSharing\Transformers;


use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;

class PostsViewTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['users','categories', 'comments'];

    public function transform(Model $model)
    {
        return [
            'id' => $model->uuid,
            'title' => $model->title,
            'slug' => $model->slug,
            'content' => $model->content,
            'privacy' => $model->privacy,
            'sponsored' => $model->sponsored,
            'sticky' => $model->sticky,
            'status' => $model->status,
            'vist' => $model->vist,
            'created_at' => $model->created_at
        ];
    }

    /**
     * @param Model $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeUsers(Model $model)
    {
        return $this->collection($model->users()->get(), new UserTransformer(), 'users');
    }

    /**
     * @param Model $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeCategories(Model $model)
    {
        return $this->collection($model->categories()->get(), new CategoriesTransformer(), 'categories');
    }

    /**
     * @param Model $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeComments(Model $model)
    {
        return $this->collection($model->comments()->get(), new PostsCommentsTransformer(), 'comments');
    }
}