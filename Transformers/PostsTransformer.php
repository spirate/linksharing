<?php
/**
 * Copyright (c) 2017.  Modulo Creado por: Kelly Salazar (VdjKelly)
 */

namespace Modules\LinkSharing\Transformers;

use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;

/**
 * Class PostsTransformer
 * @package App\Transformers
 */
class PostsTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    protected $defaultIncludes = ['users','categories'];

    /**
     * @param Posts $model
     * @return array
     */
    public function transform(Model $model)
    {
        return [
            'id' => $model->uuid,
            'title' => $model->title,
            'slug' => $model->slug,
            'content' => $model->content,
            'privacy' => $model->privacy,
            'sponsored' => $model->sponsored,
            'sticky' => $model->sticky,
            'status' => $model->status,
            'vist' => $model->vist,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at->toIso8601String(),
        ];
    }

    /**
     * @param Model $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeUsers(Model $model)
    {
        return $this->collection($model->users()->get(), new UserTransformer(), 'users');
    }

    /**
     * @param Model $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeCategories(Model $model)
    {
        return $this->collection($model->categories()->get(), new PostsCategoriesTransformer(), 'categories');
    }

}
