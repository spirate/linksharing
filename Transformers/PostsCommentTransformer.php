<?php
/**
 * Copyright (c) 2017.  Modulo Creado por: Kelly Salazar (VdjKelly)
 */

namespace Modules\LinkSharing\Transformers;

use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;


/**
 * Class PostsCommentTransformer
 * @package App\Transformers
 */
class PostsCommentTransformer extends TransformerAbstract
{
    /**
     * @param PostsComment $model
     * @return array
     */
    public function transform(Model $model)
    {
        return [
            'id' => $model->uuid,
            'body' => $model->body,
            'created_at' => $model->created_at
        ];
    }

    public function includeUsers(Model $model)
    {
        return $this->collection($model->users()->get(), new UserTransformer(), 'users');
    }

}
