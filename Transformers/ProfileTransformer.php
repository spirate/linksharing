<?php
/**
 * Copyright (c) 2017.  Modulo Creado por: Kelly Salazar (VdjKelly)
 */

namespace Modules\LinkSharing\Transformers;


use League\Fractal\TransformerAbstract;
use Illuminate\Database\Eloquent\Model;


class ProfileTransformere extends TransformerAbstract
{
    public function transform(Model $model)
    {
        return [
            'profile_avatar_user' => (string) $model->mini_profile_pic,
            'profile_avatar_timeline' => (string) $model->profile_pic,
            'profile_bio' => (string) $model->bio,
            'profile_country' => $model->country,
            'profile_province' => $model->province,
            'profile_city' => $model->city,
            'profile_brithday' => $model->brithday,
            'profile_sex' => $model->sex,
            'profile_language' => $model->language,
            'profile_twitter' => $model->twitter,
            'profile_facebook' => $model->facebook,
            'profile_instagram' => $model->instagram
        ];
    }
}
