<?php
/**
 * Copyright (c) 2017.  Modulo Creado por: Kelly Salazar (VdjKelly)
 */

namespace Modules\LinkSharing\Transformers;

use League\Fractal\TransformerAbstract;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PostsCategoriesTransformer
 * @package App\Transformers
 */
class PostsCategoriesTransformer extends TransformerAbstract
{
    /**
     * @param PostsCategories $model
     * @return array
     */
    public function transform(Model $model)
    {
        return [
            'id' => $model->uuid,
            'name' => $model->name,
            'slug' => $model->slug,
            'icon_class' => $model->icon,
            'created_at' => $model->created_at->toIso8601String(),
            'updated_at' => $model->updated_at->toIso8601String(),
        ];
    }

}
