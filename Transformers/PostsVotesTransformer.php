<?php
/**
 * Copyright (c) 2017.  Modulo Creado por: Kelly Salazar (VdjKelly)
 */

namespace Modules\LinkSharing\Transformers;


use League\Fractal\TransformerAbstract;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PostsVotesTransformer
 * @package App\Transformers
 */
class PostsVotesTransformer extends TransformerAbstract
{
    /**
     * @param PostsVotes $model
     * @return array
     */
    public function transform(Model $model)
    {
        return [
            'id' => $model->uuid,
            'created_at' => $model->created_at->toIso8601String(),
            'updated_at' => $model->updated_at->toIso8601String(),
        ];
    }

}
