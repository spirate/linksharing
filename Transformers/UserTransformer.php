<?php
/**
 * Copyright (c) 2017.  Modulo Creado por: Kelly Salazar (VdjKelly)
 */

namespace Modules\LinkSharing\Transformers;


use League\Fractal\TransformerAbstract;
use Illuminate\Database\Eloquent\Model;


class UserTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['profile'];

    public function transform(Model $model)
    {
        return [
            'uuid' => (string) $model->uuid,
            'username' => (string) $model->username,
            'name' => (string) $model->last_name,
            'email' => (string) $model->email,
            'is_follower' => (int) $model->pivot['status']
        ];
    }

    public function includeProfile(Model $model)
    {
        return $this->collection($model->perfil()->get(), new ProfileTransformere(), 'profile');
    }
}
