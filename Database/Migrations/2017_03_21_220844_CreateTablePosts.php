<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sp_posts', function (Blueprint $table){
           $table->increments('id');
           $table->string('uuid');
           $table->string('title', 240);
           $table->string('slug');
           $table->text('content');
           $table->enum('privacy', ['private', 'public']);
           $table->integer('user_id')->unsigned();
           $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
           $table->integer('categorie_id')->unsigned();
           $table->foreign('categorie_id')->references('id')->on('sp_posts_categories')->onUpdate('cascade')->onDelete('cascade');
           $table->integer('votes')->nullable();
           $table->integer('sponsored')->default(0);
           $table->integer('sticky')->default(0);
           $table->enum('status', ['revition', 'delete'])->nullable();
           $table->integer('vist')->default(0);
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sp_posts');
    }
}
