<?php

namespace Modules\LinkSharing\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;


class LinkSharingDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->categorias();

        // $this->call("OthersTableSeeder");
    }

    protected function categorias()
    {
        $categoria = [
            ['uuid' => Uuid::generate()->string, 'name' => 'Descargas', 'slug' => 'descargas', 'icon' => 'fa fa-download'],
            ['uuid' => Uuid::generate()->string, 'name' => 'Peliculas', 'slug' => 'peliculas', 'icon' => 'fa fa-file-video-o']
        ];

        foreach ($categoria as $key => $categorias) {
            DB::table('sp_posts_categories')->insert($categorias);
        }
    }
}
