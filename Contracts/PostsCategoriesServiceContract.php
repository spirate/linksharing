<?php
/**
 * Copyright (c) 2017.  Modulo Creado por: Kelly Salazar (VdjKelly)
 */

namespace Modules\LinkSharing\Contracts;

use Modules\Core\Exceptions\ValidationException;
use Modules\LinkSharing\Entities\PostsCategories;


/**
 * Class PostsCategoriesServiceContract
 * @package App\Contracts\PostsCategories
 */
interface PostsCategoriesServiceContract
{

    /**
     * @param int $limit
     * @return mixed
     */
    public function get($limit = 20);

    /**
     * @param int|string $id
     * @return PostsCategories
     */
    public function find($id);

    /**
     * @param array $attributes
     * @return PostsCategories
     * @throws ValidationException
     */
    public function create(array $attributes = []);

    /**
     * @param int|string $id
     * @param array $attributes
     * @return PostsCategories
     * @throws ValidationException
     */
    public function update($id, array $attributes = []);

    /**
     * @param int|string $id
     * @return bool
     */
    public function delete($id);

}