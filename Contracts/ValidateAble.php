<?php
/**
 * Copyright (c) 2017.  Modulo Creado por: Kelly Salazar (VdjKelly)
 */

namespace Modules\LinkSharing\Contracts;



/**
 * Interface ValidateAble
 * @package Spirate\Support\Contracts
 */
interface ValidateAble
{

    /**
     * Runs the validator and throws exception if fails
     * @param array $attributes
     * @param $rules
     * @param $messages
     * @throws ValidationException
     */
    public function runValidator(array $attributes, $rules, $messages);
}
