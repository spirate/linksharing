<?php
/**
 * Copyright (c) 2017.  Modulo Creado por: Kelly Salazar (VdjKelly)
 */

namespace Modules\LinkSharing\Contracts;

use Modules\LinkSharing\Entities\PostsVotes;
use Modules\Core\Exceptions\ValidationException;

/**
 * Class PostsVotesServiceContract
 * @package App\Contracts\PostsVotes
 */
interface PostsVotesServiceContract
{

    /**
     * @param int $limit
     * @return mixed
     */
    public function get($limit = 20);

    /**
     * @param int|string $id
     * @return PostsVotes
     */
    public function find($id);

    /**
     * @param array $attributes
     * @return PostsVotes
     * @throws ValidationException
     */
    public function create(array $attributes = []);

    /**
     * @param int|string $id
     * @param array $attributes
     * @return PostsVotes
     * @throws ValidationException
     */
    public function update($id, array $attributes = []);

    /**
     * @param int|string $id
     * @return bool
     */
    public function delete($id);

}