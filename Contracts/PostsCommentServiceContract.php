<?php
/**
 * Copyright (c) 2017.  Modulo Creado por: Kelly Salazar (VdjKelly)
 */

namespace Modules\LinkSharing\Contracts;

use Modules\LinkSharing\Entities\PostsComment;
use Modules\Core\Exceptions\ValidationException;

/**
 * Class PostsCommentServiceContract
 * @package App\Contracts\PostsComment
 */
interface PostsCommentServiceContract
{

    /**
     * @param int $limit
     * @return mixed
     */
    public function get($limit = 20);

    /**
     * @param int|string $id
     * @return PostsComment
     */
    public function find($id);

    /**
     * @param array $attributes
     * @return PostsComment
     * @throws ValidationException
     */
    public function create(array $attributes = []);

    /**
     * @param int|string $id
     * @param array $attributes
     * @return PostsComment
     * @throws ValidationException
     */
    public function update($id, array $attributes = []);

    /**
     * @param int|string $id
     * @return bool
     */
    public function delete($id);

}