<?php

namespace Modules\LinkSharing\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\User;

class PostsComments extends Model
{
    use UuidScopeTrait;

    /**
     * @var string
     */
    protected $table = "sp_posts_comments";
    /**
     * @var array
     */
    protected $fillable = [
        'uuid',
        'body',
        'post_id',
        'user_id',
        'posts_type'
    ];

    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function posts()
    {
        return $this->morphTo();
    }

    /**
     * @return string
     */
    public function getCreatedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s',$this->attributes['created_at'])->diffForHumans();
    }
}
