<?php

namespace Modules\LinkSharing\Entities;

use Illuminate\Database\Eloquent\Model;


class PostsCategories extends Model
{
    use UuidScopeTrait;

    protected $guarded = ['id'];

    protected $table = "sp_posts_categories";

    protected $fillable = ['name', 'slug', 'icon'];

    public function posts()
    {
        return $this->hasMany(Posts::class, 'id');
    }
}
