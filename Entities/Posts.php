<?php

namespace Modules\LinkSharing\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\User;

class Posts extends Model
{
    use UuidScopeTrait;

    protected $table = "sp_posts";

    protected $fillable = [
        'title',
        'slug',
        'content',
        'uuid',
        'privacy',
        'sponsored',
        'sticky',
        'status',
        'vist',
        'user_id',
        'categorie_id'
    ];

    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categories()
    {
        return $this->belongsTo(PostsCategories::class, 'categorie_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(PostsComments::class, 'post');
    }

    /**
     * @return string
     */
    public function getCreatedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s',$this->attributes['created_at'])->diffForHumans();
    }
}
