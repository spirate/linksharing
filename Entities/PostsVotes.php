<?php

namespace Modules\LinkSharing\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UuidScopeTrait;

/**
 * Class PostsVotes
 * @package App\Entities\LinkSharing
 */
class PostsVotes extends Model
{

    use UuidScopeTrait;

    protected $table = "sp_posts_votes";

    protected $fillable = ['user_id', 'post_id', 'votes'];
    
    public $timestamps = false;

    protected $guarded = ['id'];


}
