<?php
/**
 * Copyright (c) 2017.  Modulo Creado por: Kelly Salazar (VdjKelly)
 */

namespace Modules\LinkSharing\Exceptions;



use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Modules\LinkSharing\Traits\ResponseBuilder;
use Exception;

class Handler extends ExceptionHandler
{
    use ResponseBuilder;

    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
    }

}