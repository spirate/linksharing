<?php
/**
 * Copyright (c) 2017.  Modulo Creado por: Kelly Salazar (VdjKelly)
 */

namespace Modules\LinkSharing\Exceptions;

use RuntimeException;

/**
 * Clase para el manejo de Exception y validar mendiante una api o ajax los errores y enviar los
 * mensajes al en forma de json
 * Class ValidationException
 * @package Modules\LinkSharing\Exceptions
 */
class ValidationException extends RuntimeException
{
    /**
     * @var MessageBag
     */
    private $messages;

    /**
     * ValidationException constructor.
     * @param MessageBag $messageBag
     */
    public function __construct($messages)
    {
        /**
         * Si el msj es de tipo array hacemos la colecion si no lo enviamos de manera normal :)
         */
        $this->messages = is_array($messages) ? collect($messages) : $messages;
    }

    /**
     * @return MessageBag
     */
    public function getMessageBag()
    {
        return $this->messages;
    }
}