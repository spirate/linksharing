<?php
/**
 * Copyright (c) 2017.  Modulo Creado por: Kelly Salazar (VdjKelly)
 */

namespace Modules\LinkSharing\Services;

use Modules\LinkSharing\Contracts\FractalAble;
use Modules\LinkSharing\Contracts\PostsCommentServiceContract;
use League\Fractal\TransformerAbstract;
use App\Exceptions\ValidationException;
use Modules\LinkSharing\Contracts\ValidateAble;
use Modules\LinkSharing\Entities\Posts;
use Modules\LinkSharing\Entities\PostsComments;
use Modules\LinkSharing\Traits\FractalAbleTrait;
use Modules\LinkSharing\Traits\ValidateAbleTrait;


/**
 * Class PostsCommentService
 * @package App\Services
 */
class PostsCommentService implements FractalAble, ValidateAble, PostsCommentServiceContract
{

    use FractalAbleTrait, ValidateAbleTrait;

    /**
     * @var array
     */
    protected $validationCreateRules = [
        'post_id' => 'required',
        'body' => 'required|string'
    ];

    /**
     * @var array
     */
    protected $validationUpdateRules = [

    ];

    /**
     * @var array
     */
    protected $validationMessages = [

    ];

    /**
     * @var string
     */
    protected $resourceKey = "sp_posts_comments";

    /**
     * @var PostsComment
     */
    protected $model;

    protected $comments;

    /**
     * @var array
     */
    protected $includes = [];

    /**
     * PostsCommentService constructor.
     * @param PostsComment $model
     */
    public function __construct(Posts $model, PostsComments $postsComments)
    {
        $this->model = $model;
        $this->comments = $postsComments;
    }

    /**
     * @return mixed
     */
    public function setTransformer()
    {
        return app(PostsCommentTransformer::class);
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public function get($limit = 20)
    {

    }

    /**
     * @param int|string $id
     * @return PostsComment
     */
    public function find($id)
    {
        return is_int($id) ? $this->model->findOrFail($id) : $this->model->byUuid($id)->firstOrFail();
    }

    /**
     * @param array $attributes
     * @return PostsComment
     * @throws ValidationException
     */
    public function create(array $attributes = [])
    {
        $this->runValidator($attributes, $this->validationCreateRules, $this->validationMessages);
        //Existe el post o ya fue eliminado?
        $model = $this->find($attributes['post_id']);
        $comment = new PostsComments(['body' => $attributes['body'], 'post_id' => $model->id, 'user_id' => auth()->user()->id]);
        $model->comments()->save($comment);
        //Falta notificaciones y permisos de acuerdo al rango que tengo el usuario. :)
        //Algo como esto => Gate::denies('comments_posts', $model);
        //Notificaciones asi =>
        /*if ($model->user_id != auth()->user()->id)
        {
            $attributes = [
                'type_notifications' => 2,
                'model_id' => $model->uuid,
                'user_id' => $model->user_id,
                'notified_by' => auth()->user()->id,
            ];
            $this->notificationService->create($attributes);
        }*/
        return $comment;

    }

    /**
     * @param int|string $id
     * @param array $attributes
     * @return PostsComment
     * @throws ValidationException
     */
    public function update($id, array $attributes = [])
    {

    }

    /**
     * @param int|string $id
     * @return bool
     */
    public function delete($id)
    {
        $model = $this->find($id);
        $model->delete();
        return true;
    }
}
