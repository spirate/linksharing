<?php
/**
 * Copyright (c) 2017.  Modulo Creado por: Kelly Salazar (VdjKelly)
 */

namespace Modules\LinkSharing\Services;

use Modules\LinkSharing\Contracts\FractalAble;
use Modules\LinkSharing\Contracts\PostsCategoriesServiceContract;
use Modules\LinkSharing\Contracts\ValidateAble;
use Modules\LinkSharing\Entities\PostsCategories;
use League\Fractal\TransformerAbstract;
use App\Exceptions\ValidationException;
use Modules\LinkSharing\Traits\FractalAbleTrait;
use Modules\LinkSharing\Traits\ValidateAbleTrait;


/**
 * Class PostsCategoriesService
 * @package App\Services
 */
class PostsCategoriesService implements FractalAble, ValidateAble, PostsCategoriesServiceContract
{

    use FractalAbleTrait, ValidateAbleTrait;

    /**
     * @var array
     */
    protected $validationCreateRules = [
        'name' => 'required',
        'icon' => 'required'
    ];

    /**
     * @var array
     */
    protected $validationUpdateRules = [

    ];

    /**
     * @var array
     */
    protected $validationMessages = [

    ];

    /**
     * @var string
     */
    protected $resourceKey = "sp_posts_categories";

    /**
     * @var PostsCategories
     */
    protected $model;

    /**
     * @var array
     */
    protected $includes = [];

    /**
     * PostsCategoriesService constructor.
     * @param PostsCategories $model
     */
    public function __construct(PostsCategories $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function setTransformer()
    {
        return app(PostsCategoriesTransformer::class);
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public function get($limit = 20)
    {
        $model = $this->model->with($this->includes);
        if (!empty($limit)) {
            return $model->paginate($limit);
        }
        return $model->get();
    }

    /**
     * @param int|string $id
     * @return PostsCategories
     */
    public function find($id)
    {
        return is_int($id) ? $this->model->findOrFail($id) : $this->model->byUuid($id)->firstOrFail();
    }

    /**
     * @param array $attributes
     * @return PostsCategories
     * @throws ValidationException
     */
    public function create(array $attributes = [])
    {
        $this->runValidator($attributes, $this->validationCreateRules, $this->validationMessages);
        $model = $this->model->create($attributes);
        return $model;
    }

    /**
     * @param int|string $id
     * @param array $attributes
     * @return PostsCategories
     * @throws ValidationException
     */
    public function update($id, array $attributes = [])
    {
        $model = $this->find($id);
        $this->runValidator($attributes, $this->validationUpdateRules, $this->validationMessages);
        $model->fill($attributes);
        $model->save();
        return $model->fresh();
    }

    /**
     * @param int|string $id
     * @return bool
     */
    public function delete($id)
    {
        $model = $this->find($id);
        $model->delete();
        return true;
    }
}
