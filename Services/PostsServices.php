<?php namespace Modules\LinkSharing\Services;

use Modules\LinkSharing\Contracts\FractalAble;
use Modules\LinkSharing\Contracts\PostsServiceContract;
use Modules\LinkSharing\Contracts\ValidateAble;
use Modules\LinkSharing\Entities\Posts;
use Modules\LinkSharing\Entities\PostsCategories;
use Modules\LinkSharing\Traits\FractalAbleTrait;
use Modules\LinkSharing\Traits\ValidateAbleTrait;
use Modules\LinkSharing\Transformers\PostsTransformer;



/**
 *
 */
class PostsServices implements FractalAble, ValidateAble, PostsServiceContract
{
    use FractalAbleTrait, ValidateAbleTrait;

    /**
     * @var array
     */
    protected $validationCreateRules = [
        'title' => 'required',
        'categorie_id' => 'required'
    ];

    /**
     * @var array
     */
    protected $validationUpdateRules = [
        'title' => 'required',
        'categorie_id' => 'required'
    ];

    /**
     * Uso de la variable para personalizar los mensajes
     * Ejemplo 'title.required' => 'Este campo :attribute es totalmente requerido en nuestra web.!',
     * @var array
     */
    protected $validationMessages = [

    ];

    /**
     * @var string
     */
    protected $resourceKey = "sp_posts";

    /**
     * @var Posts
     */
    protected $model;
    /**
     * @var PostsCategories
     */
    protected $categories;

    /**
     * Relaciones entre modelos
     * @var array
     */
    protected $includes = ['categories', 'users'];

    /**
     * PostsService constructor.
     * @param Posts $model
     */
    public function __construct(Posts $model, PostsCategories $postsCategories)
    {
        $this->model = $model;
        $this->categories = $postsCategories;
    }

    /**
     * @return mixed
     */
    public function setTransformer()
    {
        return app(PostsTransformer::class);
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public function get($limit = 20)
    {
        $model = $this->model->with($this->includes);
        if (!empty($limit)) {
            return $model->paginate($limit);
        }
        return $model->get();
    }

    /**
     * @param int|string $id
     * @return Posts
     */
    public function find($id)
    {
        return is_int($id) ? $this->model->findOrFail($id) : $this->model->byUuid($id)->firstOrFail();
    }

    /**
     * @param array $attributes
     * @return Posts
     * @throws ValidationException
     */
    public function create(array $attributes = [])
    {
        //Corremos las validaciones por medio del attributo
        $this->runValidator($attributes, $this->validationCreateRules, $this->validationMessages);
        //Validamos si la categoria existe
        is_int($attributes['categorie_id']) ? $categories = $this->categories->findOrFail($attributes['categorie_id']) : $categories = $this->categories->byUuid($attributes['categorie_id'])->firstOrFail();
        //Falta los tags y crear su tabla para relacionar para futuras busquedas por tags

        $attributes['user_id'] = (int) auth()->guard('api')->user()->id; //User del posts
        $attributes['categorie_id'] = (int) $categories->id;
        //slug del titulo: Esto es un ejemplo =>  esto-es-un-ejemplo
        $attributes['slug'] = (string) str_slug($attributes['title']);
        $model = $this->model->create($attributes);

        return $model->fresh($this->includes);
    }

    /**
     * @param int|string $id
     * @param array $attributes
     * @return Posts
     * @throws ValidationException
     */
    public function update($id, array $attributes = [])
    {
        $model = $this->find($id);
        //Corremos las validaciones por medio de los atributos
        $this->runValidator($attributes, $this->validationUpdateRules, $this->validationMessages);
        //Validamos si la categoria existe
        is_int($attributes['categorie_id']) ? $categories = $this->categories->findOrFail($attributes['categorie_id']) : $categories = $this->categories->byUuid($attributes['categorie_id'])->firstOrFail();
        $attributes['categorie_id'] = (int) $categories->id;
        //slug del titulo: Esto es un ejemplo =>  esto-es-un-ejemplo
        $attributes['slug'] = (string) str_slug($attributes['title']);
        $model->fill($attributes);
        $model->save();
        //Refres model and Relations.
        return $model->fresh($this->includes);
    }

    /**
     * @param int|string $id
     * @return bool
     */
    public function delete($id)
    {
        $model = $this->find($id);
        $model->delete();
        return true;
    }
}
