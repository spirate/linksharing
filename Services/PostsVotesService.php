<?php

namespace Modules\LinkSharing\Services;

use Modules\LinkSharing\Contracts\FractalAble;
use Modules\LinkSharing\Contracts\PostsVotesServiceContract;
use Modules\LinkSharing\Contracts\ValidateAble;
use Modules\LinkSharing\Entities\PostsVotes;
use League\Fractal\TransformerAbstract;
use App\Exceptions\ValidationException;
use Modules\LinkSharing\Traits\FractalAbleTrait;
use Modules\LinkSharing\Traits\ValidateAbleTrait;


/**
 * Class PostsVotesService
 * @package App\Services
 */
class PostsVotesService implements FractalAble, ValidateAble, PostsVotesServiceContract
{

    use FractalAbleTrait, ValidateAbleTrait;

    /**
     * @var array
     */
    protected $validationCreateRules = [

    ];

    /**
     * @var array
     */
    protected $validationUpdateRules = [

    ];

    /**
     * @var array
     */
    protected $validationMessages = [

    ];

    /**
     * @var string
     */
    protected $resourceKey = "sp_posts_votes";

    /**
     * @var PostsVotes
     */
    protected $model;

    /**
     * @var array
     */
    protected $includes = [];

    /**
     * PostsVotesService constructor.
     * @param PostsVotes $model
     */
    public function __construct(PostsVotes $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function setTransformer() : TransformerAbstract
    {
        return app(PostsVotesTransformer::class);
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public function get($limit = 20)
    {
        $model = $this->model->with($this->includes);
        if (!empty($limit)) {
            return $model->paginate($limit);
        }
        return $model->get();
    }

    /**
     * @param int|string $id
     * @return PostsVotes
     */
    public function find($id)
    {
        return is_int($id) ? $this->model->findOrFail($id) : $this->model->byUuid($id)->firstOrFail();
    }

    /**
     * @param array $attributes
     * @return PostsVotes
     * @throws ValidationException
     */
    public function create(array $attributes = [])
    {
        $this->runValidator($attributes, $this->validationCreateRules, $this->validationMessages);
        $model = $this->model->create($attributes);
        return $model;
    }

    /**
     * @param int|string $id
     * @param array $attributes
     * @return PostsVotes
     * @throws ValidationException
     */
    public function update($id, array $attributes = [])
    {
        $model = $this->find($id);
        $this->runValidator($attributes, $this->validationUpdateRules, $this->validationMessages);
        $model->fill($attributes);
        $model->save();
        return $model->fresh();
    }

    /**
     * @param int|string $id
     * @return bool
     */
    public function delete($id)
    {
        $model = $this->find($id);
        $model->delete();
        return true;
    }
}
