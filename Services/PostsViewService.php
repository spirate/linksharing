<?php
/**
 * Copyright (c) 2017.  Modulo Creado por: Kelly Salazar (VdjKelly)
 */

namespace Modules\LinkSharing\Services;


use Modules\LinkSharing\Contracts\FractalAble;
use Modules\LinkSharing\Entities\Posts;
use Modules\LinkSharing\Traits\FractalAbleTrait;
use Modules\LinkSharing\Transformers\PostsViewTransformer;

/**
 * Class PostsViewService
 * @package Modules\LinkSharing\Services
 */
class PostsViewService implements FractalAble
{
    use FractalAbleTrait;

    /**
     * @var Posts
     */
    protected $model;

    /**
     * @var
     */
    protected $resourceKey;

    /**
     * @var array
     */
    protected $includes = ['users', 'comments', 'categories'];

    /**
     * PostsViewService constructor.
     * @param Posts $posts
     */
    public function __construct(Posts $posts)
    {
        $this->model = $posts;
    }

    /**
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function setTransformer()
    {
        // TODO: Implement setTransformer() method.
        return app(PostsViewTransformer::class);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        is_int($id) ? $model = $this->model->findOrFail($id) : $model = $this->model->byUuid($id)->firstOrFail();
        $posts = $model->with($this->includes)->where('uuid', $id)->get();
        return $posts;
    }

}