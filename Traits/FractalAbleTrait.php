<?php namespace Modules\LinkSharing\Traits;
use League\Fractal\Serializer\SerializerAbstract;
use League\Fractal\Serializer\DataArraySerializer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * class FractalAbleTrait Para los Transformers al momento de solicitar datos en una API 
 * [setSerializer description]
 * @return {SerializerAbstract [description]
 */
trait FractalAbleTrait
{


    /**
     * @return SerializerAbstract
     */
    public function setSerializer() : SerializerAbstract
    {
        return app(DataArraySerializer::class);
    }


    /**
     * @param Collection $collection
     * @param String|null $resourceKey
     * @return array
     */
    public function transformCollection(Collection $collection, String $resourceKey = null)
    {
        return fractal()
            ->collection($collection, $this->setTransformer(), $resourceKey)
            ->serializeWith($this->setSerializer())
            ->toArray();
    }


    /**
     * @param Model $item
     * @param String|null $resourceKey
     * @return array
     */
    public function transformItem(Model $item, String $resourceKey = null)
    {
        return fractal()
            ->item($item, $this->setTransformer(), $resourceKey)
            ->serializeWith($this->setSerializer())
            ->toArray();
    }


    /**
     * @param LengthAwarePaginator $paginator
     * @param String|null $resourceKey
     * @return array
     */
    public function transformPaginator(LengthAwarePaginator $paginator, String $resourceKey = null)
    {
        return fractal()
            ->collection($paginator->getCollection(), $this->setTransformer(), $resourceKey)
            ->serializeWith($this->setSerializer())
            ->paginateWith(new IlluminatePaginatorAdapter($paginator))
            ->toArray();
    }


    /**
     * @param $resource
     * @param array $meta
     * @return array
     */
    public function transform($resource, $meta = [])
    {
        if ($resource instanceof Model) {
            return $this->transformItem($resource, $this->resourceKey, $meta);
        }

        if ($resource instanceof Collection) {
            return $this->transformCollection($resource, $this->resourceKey, $meta);
        }

        if ($resource instanceof LengthAwarePaginator) {
            return $this->transformPaginator($resource, $this->resourceKey, $meta);
        }

    }
}
