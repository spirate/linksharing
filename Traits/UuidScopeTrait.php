<?php namespace Modules\LinkSharing\Traits;
use Webpatser\Uuid\Uuid;


/**
 * Class UuidScopeTrait
 * @package Spirate\Support\Traits
 */
trait UuidScopeTrait
{

    /**
     * @param $query
     * @param $uuid
     * @return mixed
     */
    public function scopeByUuid($query, $uuid)
    {
        return $query->where('uuid', $uuid);
    }

    /**
     * Boot function from laravel.
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->uuid = Uuid::generate()->string;
        });
    }
}
