<?php namespace Modules\LinkSharing\Traits;



use Illuminate\Contracts\Validation\Factory as Validator;
use Modules\Core\Exceptions\ValidationException;

/**
 * [Validacion Request]
 * @param  array   $attributes [atributos enviados por el usuario en un form tipo post]
 * @param  [type]  $rules      [validaciones de los diferentes atributos enviados ejemplo: required]
 * @param  [type]  $messages   [mensaje de error a imprimir]
 * @return {[type]             [retonamos una excepcion]
 */
trait ValidateAbleTrait
{
    /**
     * @param array $attributes
     * @param $rules
     * @param $messages
     */
    public function runValidator(array $attributes, $rules, $messages)
    {
        $validator = app(Validator::class)->make($attributes, $rules, $messages);
        if ($validator->fails()) {
            throw new ValidationException($validator->getMessageBag());
        }
    }
}
